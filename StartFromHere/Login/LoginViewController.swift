//
//  LoginViewController.swift
//  StartFromHere
//
//  Created by Anton on 19.10.20.
//

import Foundation
import UIKit

struct UserModel {
    let username: String
    let cookie: String
}

extension UserModel {
    init?(jsonDictionary: [String: AnyObject]) {
        guard let parsedName = jsonDictionary["username"] as? String else {
            return nil
        }
        username = parsedName
        
        guard let parsedCookie = jsonDictionary["cookie"] as? String else {
            return nil
        }
        cookie = parsedCookie
    }
}

class LoginViewController: UIViewController, NavigatorPage {
    typealias ResultFormat = UserModel?
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var viewModel:LoginViewModel!
    var exitHandler: ((ResultFormat) -> Void)?
    
    override func viewDidLoad() {
        if (self.viewModel == nil) {
            return
        }
        self.viewModel.didUpdateModel = {[unowned self] in
            self.updateUI()
        }
        self.viewModel.didLogIn = {[unowned self] model in
            DispatchQueue.main.async {
                self.dismiss(animated: true) { self.exitHandler?(model) }
            }
            
        }
        self.viewModel.didFinish = {[unowned self] in
            DispatchQueue.main.async {
                self.dismiss(animated: true) { self.exitHandler?(nil) }
            }
        }
        initButtons()
        updateUI()
    }

    func initButtons() {
        btnBack.addTarget(self, action: #selector(backDidTouch), for: .touchUpInside)
        btnLogin.addTarget(self, action: #selector(loginDidTouch), for: .touchUpInside)
    }
    
    func updateUI() {
        tfUsername.text = viewModel.username
        tfPassword.text = viewModel.password
    }

    @objc func backDidTouch() {
        self.viewModel.back()
    }
    
    @objc func loginDidTouch() {
        self.viewModel.logIn()
    }
}



//extension LoginViewController: LoginWithUser, Networkable {
//    func loginWithUsername(_ username: String, and password: String) {
//        if let url = URL(string: "http://login") {
//            let params: [String: String] = ["username": username, "password": password]
//            self.loader.requestFromURL(url, with: params) {success, result in
//
//            }
//        }
//    }
//
//}

