//
//  LoginViewModel.swift
//  StartFromHere
//
//  Created by Anton on 20.10.20.
//

import Foundation

protocol LoginViewModelProtocol {
    var didUpdateModel: (() -> Void)? { get set }
    var didLogIn: ((UserModel) -> Void)? { get set }
    var didFinish: (() -> Void)? { get set }
    var username: String { get }
    var password: String { get }
    func logIn()
    func back()
}

struct LoginViewModel: LoginViewModelProtocol, LogingIn {
    var didUpdateModel: (() -> Void)?
    var didLogIn: ((UserModel) -> Void)?
    var didFinish: (() -> Void)?
    var username: String = ""
    var password: String = ""
    
    func logIn() {
        self.login(username: username, password: password) {result in
            switch (result) {
            case .Failure(let error):
                print("Login failed \(error)")
            case .Success(let model):
                didLogIn?(model)
            }
        }
    }
    
    func back() {
        didFinish?()
    }
    
    
}
