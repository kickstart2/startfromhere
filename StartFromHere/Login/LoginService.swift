//
//  LoginService.swift
//  StartFromHere
//
//  Created by Anton on 20.10.20.
//

import Foundation

let baseURL = URL(string: "http://127.0.0.1:8081/")!

struct LoginResource:NetworkJSONResourceType {
    typealias Model = UserModel
    let url:URL
    init(username: String, password: String) {
        url = baseURL.appendingPathComponent("login")
    }
    
    func modelFrom(_ jsonDictionary: [String : AnyObject]) -> Model? {
        return Model.init(jsonDictionary: jsonDictionary)
    }
}

protocol LogingIn {
    func login(username: String, password: String, completion: @escaping (Result<UserModel>) -> Void)
}

extension LogingIn {
    func login(username: String, password: String, completion: @escaping (Result<UserModel>) -> Void) {
        let loginResource = LoginResource(username: username, password: password)
        let service = NetworkJSONService<LoginResource>()
        service.fetch(loginResource) {result in
            completion(result)
        }
    }
}

//protocol LoginService {
//    var url:URL { get }
//    var loader: Loader { get }
//    func loginWithUsername(_ username: String, and password: String, _ callback: (UserModel) -> Void)
//}
//
//extension LoginService {
//    func loginWithUsername(_ username: String, and password: String, _ callback: @escaping (UserModel) -> Void) {
//        let resource = LoginServiceResource(url: url) { model in
//            callback(model)
//        }
//        loader.loadResource(resource)
//    }
//}
//
//struct LoginServiceResource:NetworkResource {
//    typealias Model = UserModel
//    var url: URL
//    var complete: (Model) -> Void
//
//    func parse<Response>(_ data: Response) -> Model {
//        return Model(username: data, cookie: "")//(username: data, cookie: data)
//    }
//}


