//
//  ViewController.swift
//  StartFromHere
//
//  Created by Anton on 17.10.20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        performSegue(withIdentifier: <#T##String#>, sender: <#T##Any?#>)
    }
    
    @IBAction func loginDidTouch(sender: Any) {
        performSegue(withIdentifier: .SegueLogin, sender: self)
    }

}

extension ViewController: SegueIdentifierType {
    enum SegueIdentifier: String {
        case SegueLogin
//        case SegueMain
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
        case .SegueLogin:
            if let vcLogin = segue.destination as? LoginViewController {
                vcLogin.viewModel = LoginViewModel()
                vcLogin.exitHandler = {model in
                    print("Login finished with result \(String(describing: model))") }
            }
//        case .SegueMain:
//            break
        }
        
    }
}

protocol NavigatorPage {
    associatedtype ResultFormat
    var exitHandler:((ResultFormat) -> Void)? { get set }
}

//protocol Loader {
//    func requestFromURL(_ url:URL, with params:[String: String], _ callback: (Bool, String) -> Void)
////    func loadResource<T: NetworkResource>(_ resource: T)
//}
//

//
//protocol LoginWithUser: Networkable {
//    func loginWithUsername(_ username: String, and password: String)
//}


//protocol Networkable {
//    var loader:Loader { get }
//}

//extension Networkable {
//    var loader: Loader {
//        StandaloneLoader()
//    }
//}

//protocol StoreUserSession {
//    func storeUser(_ user: UserModel)
//    func currentUser() -> UserModel
//}

//protocol NetworkResource {
//    associatedtype Model
//    associatedtype Response
//    var url:URL { get }
//    var complete: (_ model: Model) -> Void { get set }
//    func modelFrom(jsonDictionary jsonDictionary: Response) -> Model
//}
//
//
//
//struct StandaloneLoader: Loader {
//    func requestFromURL(_ url: URL, with params: [String : String], _ callback: (Bool, String) -> Void) {
//        callback(true, "standalone abc")
//    }
//    func loadResource<T: NetworkResource>(_ resource: T) {
//        resource.complete(resource.parse("standalone abc"))
//    }
//}
//
//struct RemoteLoader: Loader {
//    func requestFromURL(_ url: URL, with params: [String : String], _ callback: (Bool, String) -> Void) {
//        callback(true, "remote abc")
//    }
//    func loadResource<T: NetworkResource>(_ resource: T) {
//        resource.complete(resource.parse("remote abc"))
//    }
//}
