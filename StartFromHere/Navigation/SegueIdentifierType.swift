//
//  SegueIdentifierType.swift
//  StartFromHere
//
//  Created by Anton on 21.10.20.
//

import Foundation
import UIKit

protocol SegueIdentifierType {
    associatedtype SegueIdentifier: RawRepresentable
}

extension SegueIdentifierType where Self: UIViewController, SegueIdentifier.RawValue == String   {
    func performSegue(withIdentifier identifier: SegueIdentifier, sender: Any?) {

        performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }

    func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier, let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
            fatalError("Could not handle segue identifier \(String(describing: segue.identifier)) for view controller of type \(type(of: self)).")
        }
        return segueIdentifier
    }
}
