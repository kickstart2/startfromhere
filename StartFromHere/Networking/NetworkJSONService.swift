//
//  NetworkService.swift
//  StartFromHere
//
//  Created by Anton on 20.10.20.
//

import Foundation

protocol ResourceServiceType {
    associatedtype Resource: ResourceType
    init()
    func fetch(_ resource:Resource, completion: @escaping (Result<Resource.Model>) -> Void)
}

protocol NetworkJSONResourceType: NetworkResourceType, JSONResourceType {}


protocol URLSessionType {
    func performRequest(_ request:URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: URLSessionType {
    func performRequest(_ request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        dataTask(with: request, completionHandler: completion).resume()
    }
}

enum NetworkJSONServiceError: Error {
    case NetworkError(error: Error)
    case NoData
}

struct NetworkJSONService<Resource: NetworkJSONResourceType> {
    private let session: URLSessionType
    
    init(session: URLSessionType) {
        self.session = session
    }
    
    private func resultFrom(_ resource: Resource, data:Data?, response: URLResponse?, error: Error?) -> Result<Resource.Model> {
        if let error = error {
            return .Failure(NetworkJSONServiceError.NetworkError(error: error))
        }
        
        guard let data = data else {
            return .Failure(NetworkJSONServiceError.NoData)
        }
        
        return resource.resultFrom(data)
    }
}

extension NetworkJSONService: ResourceServiceType {
    init() {
        self.init(session: URLSession.shared)
    }
    
    func fetch(_ resource: Resource, completion: @escaping (Result<Resource.Model>) -> Void) {
        let request = resource.urlRequest()
        session.performRequest(request) { (data, _, error) in
            completion(self.resultFrom(resource, data: data, response: nil, error: error))
        }
    }
}


