//
//  NetworkResourceType.swift
//  StartFromHere
//
//  Created by Anton on 20.10.20.
//

import Foundation

protocol NetworkResourceType {
    var url:URL { get }
    var method: String { get }
    var headers: [String: String]? { get }
    var body: AnyObject? { get }
}

extension NetworkResourceType {
    var method:String { return "GET" }
    var headers: [String: String]? { return nil }
    var body: AnyObject? { return nil }
}

extension NetworkResourceType {
    func urlRequest() -> URLRequest {
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        request.httpMethod = method
        if let body = body {
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
        }
        return request
    }
}
