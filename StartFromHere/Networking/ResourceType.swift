//
//  ResourceType.swift
//  StartFromHere
//
//  Created by Anton on 20.10.20.
//

import Foundation

enum Result<T> {
    case Success(T)
    case Failure(Error)
}

enum ParsingError: Error {
    case InvalidJSONData
    case CannotParseJSONDictionary
    case CannotParseJSONArray
    case UnsupportedType
}

protocol ResourceType {
    associatedtype Model
}

protocol JSONResourceType: ResourceType {
    func modelFrom(_ jsonDictionary: [String:AnyObject]) -> Model?
    func modelFrom(_ jsonArray: [AnyObject]) -> Model?
}

extension JSONResourceType {
    func modelFrom(_ jsonDictionary: [String:AnyObject]) -> Model? { return nil }
    func modelFrom(_ jsonArray: [AnyObject]) -> Model? { return nil }
}

extension JSONResourceType {
    func resultFrom(_ data:Data) -> Result<Model> {
        guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
            return .Failure(ParsingError.InvalidJSONData)
        }
        
        if let jsonArray = jsonObject as? [AnyObject] {
            return resultFrom(jsonArray)
        }
        
        if let jsonDictionary = jsonObject as? [String: AnyObject] {
            return resultFrom(jsonDictionary)
        }
        
        return .Failure(ParsingError.UnsupportedType)
    }
    
    private func resultFrom(_ jsonArray: [AnyObject]) -> Result<Model> {
        if let parseResults = modelFrom(jsonArray) {
            return .Success(parseResults)
        }
        return .Failure(ParsingError.CannotParseJSONArray)
    }
    
    private func resultFrom(_ jsonDictionary: [String: AnyObject]) -> Result<Model> {
        if let parseResults = modelFrom(jsonDictionary) {
            return .Success(parseResults)
        }
        return .Failure(ParsingError.CannotParseJSONDictionary)
    }
}
